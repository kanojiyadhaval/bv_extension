<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\Base\Helper;

class Data extends Main
{

    protected $_configSectionId = 'plumbase';


    public function moduleEnabled($store = null)
    {
        return true;
    }


    public function isAdminNotificationEnabled()
    {
        $m = 'Mage_Admin'.'Not'.'ification';
        return //(($module = Mage::getConfig()->getModuleConfig($m))
            //&& ($module->is('active', 'true'))
            //&& !Mage::getStoreConfig($this->_getAd().'/'.$m));
            !$this->scopeConfig->isSetFlag($this->_getAd().'/'.$m, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    protected function _getAd()
    {
        return 'adva'.'nced/modu'.
            'les_dis'.'able_out'.'put';
    }


}
