<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/
namespace Brainvire\Base\Observer;
use Magento\Framework\Event\ObserverInterface;
/**
 * Base observer
 */
class Reindex implements ObserverInterface
{
    
    protected $_product;

    
    public function __construct(
    \Brainvire\Base\Model\Product $product
    ) {
        $this->_product = $product;
    }

    /**
     * Predispath admin action controller
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_product->reindex();
    }

}