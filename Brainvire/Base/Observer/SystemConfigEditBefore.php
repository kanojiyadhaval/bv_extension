<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\Base\Observer;

/**
 * Base observer
 */
class SystemConfigEditBefore extends AbstractSystemConfig
{
    /**
     * Predispath admin action controller
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $section = $this->_getSection($observer);
        if (!$section) {
            return;
        }

        $product = $this->_getProductBySection($section);
        if (!$product->getSession()) {
            if ($s = $product->loadSession()) {
                $this->_objectManager->get('\Magento\Config\Model\ResourceModel\Config')
                    ->saveConfig($product->getSessionKey(), $s, 'default', 0);

                // clear the config cache
                $this->_cacheTypeList->cleanType('config');
                $this->_eventManager->dispatch('adminhtml_cache_refresh_type', ['type' => 'config']);
            }
        } else {
            $product = $this->_objectManager->create('\Brainvire\Base\Model\Product')->load($product->getName());
            if (!$product->isInStock() || !$product->isCached()) {
                $product->checkStatus();
            }
        }
        if (!$product->isInStock()) {
            $product->disable();
        }
        if (!$product->isInStock()) {
            $this->_messageManager->addError($product->getDescription());
        }


    }
}