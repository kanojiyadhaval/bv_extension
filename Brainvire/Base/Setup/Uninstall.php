<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\Base\Setup;

/* Uninstall Base */
class Uninstall extends AbstractUninstall
{

    protected $_tables = ['plumbase_product'];
    protected $_pathes = ['/app/code/Brainvire/Base'];
}