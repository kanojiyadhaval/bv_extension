<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Block\Adminhtml\System\Config\Form;

class Sortable extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_helper;

    public function _construct()
    {
        parent::_construct();

        $this->_helper = \Magento\Framework\App\ObjectManager::getInstance()->get('Brainvire\SocialLogin\Helper\Data');
        $this->setTemplate('system/config/sortable.phtml');
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $this->element = $element;
        return $this->toHtml();
    }

    public function getButtons()
    {
        return $this->_helper->getButtons();
    }

    public function getPreparedButtons($part)
    {
        return $this->_helper->getPreparedButtons($part);
    }

}