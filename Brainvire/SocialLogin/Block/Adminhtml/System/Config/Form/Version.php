<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Block\Adminhtml\System\Config\Form;

class Version extends \Brainvire\Base\Block\Adminhtml\System\Config\Form\Version
{
    protected $_wikiLink = '';
    protected $_moduleName = 'Twitter & Facebook Login';
}