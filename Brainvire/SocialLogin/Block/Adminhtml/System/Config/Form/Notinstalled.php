<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/
namespace Brainvire\SocialLogin\Block\Adminhtml\System\Config\Form;

class Notinstalled extends \Magento\Config\Block\System\Config\Form\Field
{
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
		// $config 		= \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Config\Model\Config');
    	// $moduleNode	= $config->getDataByPath('config');
        // $name 		= $moduleNode->name;
        $name 			= 'Twitter & Facebook Login';
        $url 			= '';

        return '<div class="psloginfree-notinstalled" style="padding:10px;background-color:#fff;border:1px solid #ddd;margin-bottom:7px;">'.
			__('The free version of "%1" extension does not include this network. Please <a href="%2" target="_blank">upgrade to Social Login Pro magento extension</a> in order to receive 50+ social login networks.', $name, $url)
		.'</div>';
    }		            
}