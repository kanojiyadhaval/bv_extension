<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Block;

class General extends \Magento\Framework\View\Element\Template
{
	protected function _toHtml()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('Brainvire\SocialLogin\Helper\Data');
        if(!$helper->moduleEnabled()) {
            return;
        }

        $moduleName = $this->getRequest()->getModuleName();

        // Set current store.
        if($moduleName != 'pslogin') {
            $currentStoreId = $objectManager->get('Magento\Store\Model\StoreManager')->getStore()->getId();
            $helper->refererStore($currentStoreId);
        }

        // Set referer.
        if(!$objectManager->get('Magento\Customer\Model\Session')->isLoggedIn()) {
            $skipModules = $helper->getRefererLinkSkipModules();
            if($this->getRequest()->getActionName() != 'noRoute' && !in_array($moduleName, $skipModules)) {
                $referer = $objectManager->get('Magento\Framework\Url\Helper\Data')->getCurrentBase64Url();
                $helper->refererLink($referer);
            }
        }
        
        return parent::_toHtml();
    }
}