<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Block;

class Buttons extends \Magento\Framework\View\Element\Template
{
    protected $_countFullButtons = 6;

    public function getHelper()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Brainvire\SocialLogin\Helper\Data');
    }

    public function getPreparedButtons($part = null)
    {
        return $this->getHelper()->getPreparedButtons($part);
    }

    public function hasButtons()
    {
        return (bool)$this->getPreparedButtons();
    }

    public function showLoginFullButtons()
    {
        $visible = $this->getPreparedButtons('visible');
        return count($visible) <= $this->_countFullButtons;
    }

    public function showRegisterFullButtons()
    {
        return $this->showFullButtons();
    }

    public function showFullButtons()
    {
        $all = $this->getPreparedButtons();
        return count($all) <= $this->_countFullButtons;
    }

    public function setFullButtonsCount($count)
    {
        if(is_numeric($count) && $count >= 0) {
            $this->_countFullButtons = $count;
        }
        return $this;
    }

    /*public function isAutocompleteDisabled()
    {
        return true;
    }*/

}