<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Model;

class Googleplus extends Account
{
	protected $_type = 'googleplus';
	
        protected $_url = 'https://accounts.google.com/o/oauth2/auth';

	protected $_fields = [
                            'user_id' => 'id',
		            'firstname' => 'name',
		            'lastname' => 'name',
		            'email' => 'email',
		           // 'dob' => 'birthday',
                            'gender' => 'gender',
                            'photo' => 'picture',
			];

	protected $_buttonLinkParams = [
					'scope' => 'email',
                                        'display' => 'popup',
                                    ];

    protected $_popupSize = [650, 350];
    protected $token = null;

	public function _construct()
    {      
        parent::_construct();
        
        $this->_buttonLinkParams = array_merge($this->_buttonLinkParams, [
            'client_id'     => $this->_applicationId,
            'redirect_uri'  => $this->_redirectUri,
            'response_type' => $this->_responseType
        ]);
    }

    public function loadUserData($response)
    { 
      	if(empty($response)) {
    		return false;
    	}

        $data = [];

        if(empty($this->token)) {
            $this->fetchAccessToken($response);
        } else if($this->isAccessTokenExpired()) {
            $this->refreshAccessToken();
        }
        if (isset($this->token->access_token)) {
            $params = [
                'access_token'  => $this->token->access_token,
                'fields'        => implode(',', $this->_fields)
            ];
    
            if($response = $this->_call('https://www.googleapis.com/oauth2/v2/userinfo', $params)) {
                $data = json_decode($response, true);
                 $this->_setLog($response, true);
                 $this->_setLog($this->token, true);
            }
        
            $this->_setLog($data, true);
        }
       
        if(!$this->_userData = $this->_prepareData($data)) {
        	return false;
        }

        $this->_setLog($this->_userData, true);

        return true;
    }

    protected function _prepareData($data)
    {
    	if(empty($data['id'])) {
    		return false;
    	}

        return parent::_prepareData($data);
    } 
    
    public function getAccessToken($code)
    {
        if(empty($this->token)) {
            $this->fetchAccessToken($code);
        } else if($this->isAccessTokenExpired()) {
            $this->refreshAccessToken();
        }

        return json_encode($this->token);
    } 
    protected function fetchAccessToken($code)
    {
        if(empty($code)) {
            return false;
        }
        $params = [
            'code' => $code,
            'redirect_uri' => $this->_redirectUri,
            'client_id' => $this->_applicationId,
            'client_secret' => $this->_secret,
            'grant_type' => 'authorization_code',
        ];
       
        $response = $this->_call('https://accounts.google.com/o/oauth2/token', $params, 'POST');
        $response = json_decode($response);
        if(!isset($response->error)){
            $response->created = time();
            $this->token = $response;
        }
        
    } 
    protected function isAccessTokenExpired() {
        // If the token is set to expire in the next 30 seconds.
        $expired = ($this->token->created + ($this->token->expires_in - 30)) < time();
        return $expired;
    } 
    
    protected function refreshAccessToken()
    {
        if(empty($this->token->refresh_token)) {
           return false;
        }
        $params = [
            'client_id' => $this->_applicationId,
            'client_secret' => $this->_secret,
            'refresh_token' => $this->token->refresh_token,
            'grant_type' => 'refresh_token',
        ];
        
        $response = $this->_call('https://accounts.google.com/o/oauth2/token', $params, 'POST');
        $response = json_decode($response);
       

        $this->token->access_token = $response->access_token;
        $this->token->expires_in = $response->expires_in;
        $this->token->created = time();
    } 
    

}