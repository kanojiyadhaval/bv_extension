<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Model\Config\Source;

class RedirectTo implements \Magento\Framework\Option\ArrayInterface
{

    protected $_options = null;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_getOptions();
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $options = [];
        foreach ($this->_getOptions() as $option) {
            $options[ $option['value'] ] = $option['label'];
        }

        return $options;
    }

    protected function _getOptions()
    {
        if(null === $this->_options) {
            $options = [
                ['value' => '__referer__',     'label' => __('Current page')],
                ['value' => '__custom__',      'label' => __('Redirect to Custom URL')],
                ['value' => '__none__',        'label' => __('None')],
                ['value' => '__dashboard__',   'label' => __('My Account Page')],
            ];

            $this->_options = $options;
        }

        return $this->_options;
    }

}