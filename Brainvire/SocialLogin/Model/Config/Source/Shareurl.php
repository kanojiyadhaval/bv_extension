<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Model\Config\Source;

class ShareUrl implements \Magento\Framework\Option\ArrayInterface
{

    protected $_options = null;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_getOptions();
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $options = [];
        foreach ($this->_getOptions() as $option) {
            $options[ $option['value'] ] = $option['label'];
        }

        return $options;
    }

    protected function _getOptions()
    {
        if(null === $this->_options) {
            $invitationsEnabled = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Brainvire\SocialLogin\Helper\Data')
                ->moduleInvitationsEnabled();

            $options = [
                ['value' => '__custom__',  'label' => __('Redirect to Custom URL')],
                ['value' => '__invitations'. (!$invitationsEnabled? 'off' : '') .'__', 'disabled' => 'disabled', 'label' => __('Plumrocket Invitations Promo Page'. (!$invitationsEnabled? ' (Not installed)' : ''))],
                ['value' => '__none__',    'label' => __('---')],
            ];

            $items = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Cms\Model\Page')
                ->getCollection()
                ->getItems();

            foreach ($items as $item) {
                if($item->getId() == 1) continue;
                $options[] = ['value' => $item->getId(), 'label' => $item->getTitle()];
            }

            $this->_options = $options;
        }

        return $this->_options;
    }

}