<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Controller\Account;

class DoUse extends \Brainvire\SocialLogin\Controller\AbstractAccount
{
    
    public function execute()
    {
        $session = $this->_getSession();
        if ($session->isLoggedIn() && !$this->getRequest()->getParam('call')) {
            return $this->_windowClose();
        }

        $type = $this->getRequest()->getParam('type');
        $className = 'Brainvire\SocialLogin\Model\\'. ucfirst($type);
        if(!$type || !class_exists($className)) {
            return $this->_windowClose();
        }

        $model = $this->_objectManager->get($className);

        if(!$this->_getHelper()->moduleEnabled() || !$model->enabled()) {
            return $this->_windowClose();
        }
 
        if($call = $this->getRequest()->getParam('call')) {
            
            $this->_getHelper()->apiCall([
                'type'      => $type,
                'action'    => $call,
            ]);
        }else{
            $this->_getHelper()->apiCall(null);
        }
        
        switch($model->getProtocol()) {

            case 'OAuth':
                
                if($link = $model->getProviderLink()) {
                   
                    return $this->_redirect($link);
                }else{
                    $this->getResponse()->setBody(__('This Login Application was not configured correctly. Please Try Again Later.'));
                }
                break;

            case 'OpenID':
            case 'BrowserID':
            default:
                return $this->_windowClose();
        }
        
    }

}