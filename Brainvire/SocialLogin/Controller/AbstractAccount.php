<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Controller;

use Magento\Framework\Controller\ResultFactory;

abstract class AbstractAccount extends \Magento\Framework\App\Action\Action
{

    protected function _windowClose()
    {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
            $this->getResponse()->setBody(json_encode([
                'windowClose' => true
            ]));
        }else{
            $this->getResponse()->setBody('<script type="text/javascript">window.close();</script>');
        }
        // $this->getResponse()->setBody('<script type="text/javascript">if(window.name == "pslogin_popup") { window.close(); }</script>');
    }

    protected function _dispatchRegisterSuccess($customer)
    {
        $this->_eventManager->dispatch(
            'customer_register_success',
            ['account_controller' => $this, 'customer' => $customer]
        );
    }

    protected function _getSession()
    {
        return $this->_objectManager->get('Magento\Customer\Model\Session');
    }

    protected function _getUrl($url, $params = [])
    {
        return $this->_url->getUrl($url, $params);
    }

    protected function _getHelper()
    {
        return $this->_objectManager->get('Brainvire\SocialLogin\Helper\Data');
    }

    /*protected function _redirectUrl($url)
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)
            ->setUrl($url);
        return $resultRedirect;
    }*/

}