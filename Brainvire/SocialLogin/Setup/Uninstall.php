<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Setup;

class Uninstall extends \Brainvire\Base\Setup\AbstractUninstall
{
	protected $_configSectionId = 'psloginfree';
	protected $_tables = ['plumrocket_sociallogin_account'];
	protected $_pathes = ['/app/code/Brainvire/SocialLogin'];
}