<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'brainvire_sociallogin'
         */
		$table = $installer->getConnection()
		    ->newTable($installer->getTable('brainvire_sociallogin'))
		    ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
		        'identity'  => true,
		        'unsigned'  => true,
		        'nullable'  => false,
		        'primary'   => true,
		        ], 'Id')
		    ->addColumn('type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 30, [
		        'nullable'  => false,
		        ], 'Login type')
		    // ->addColumn('user_id', \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT, null, [
		    ->addColumn('user_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [
		        // 'unsigned'  => true,
		        'nullable'  => false,
		        // 'default'   => '0',
		        ], 'User Id')
		    ->addColumn('customer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
		        'unsigned'  => true,
		        'nullable'  => false,
		        'default'   => '0',
		        ], 'Customer Id')
		    ->addIndex($installer->getIdxName('brainvire_sociallogin', ['type']), ['type'])
		    ->addIndex($installer->getIdxName('brainvire_sociallogin', ['user_id']), ['user_id'])
		    ->addIndex($installer->getIdxName('brainvire_sociallogin', ['customer_id']), ['customer_id'])
		    ->addForeignKey($installer->getFkName('brainvire_sociallogin', 'customer_id', 'customer_entity', 'entity_id'),
		        'customer_id', $installer->getTable('customer_entity'), 'entity_id',
		        \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
		        \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE)
		    ->setComment('Social Login Customer');
		$installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}