<?php
/*
@ Company : Brainvire Infotech.
@ author : dhaval.kanojiya@brainvire.com
@ Desc : Socail Login
*/

namespace Brainvire\SocialLogin\Observer;

use Brainvire\SocialLogin\Helper\Data as HelperData;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class LoginObserver implements ObserverInterface
{
    protected $_helper;
    protected $_session;

    public function __construct(
        HelperData $helper,
        Session $customerSession
    ) {
        $this->_helper = $helper;
        $this->_session = $customerSession;
    }

    public function execute(Observer $observer)
    {
        if(!$this->_helper->moduleEnabled()) {
            return;
        }

        // Set redirect url.
        $redirectUrl = $this->_helper->getRedirectUrl('login');
        $this->_session->setBeforeAuthUrl($redirectUrl);
    }
}
