define([
    'jquery',
    'fancybox'
    ], function ($, fancybox) {
    "use strict";

    return {
        displayContent: function(prodUrl) {
            if (!prodUrl.length) {
                return false;
            }
            var url = '/brainvire_quickview/index/updatecart';
            
            $.fancybox.open({
                href : prodUrl,
                type : 'iframe',
                padding : 5,
                helpers     : {
                    overlay : {
                        closeClick: false
                    }
                },
            });
        }
    };

});



	