var config = {
    map: {
        '*': {
            fancybox: 'Brainvire_Quickview/js/jquery.fancybox',
            brainvire_quickview: 'Brainvire_Quickview/js/brainvire_quickview'
        }
    },
    shim: {
         fancybox: {
            deps: ['jquery']
        }
    }
};