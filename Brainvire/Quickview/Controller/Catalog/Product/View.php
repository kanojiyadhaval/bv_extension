<?php

namespace Brainvire\Quickview\Controller\Catalog\Product;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends \Magento\Catalog\Controller\Product\View
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Brainvire_Quickview::test.phtml');
    }
}
