<?php
namespace Brainvire\Quickview\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddUpdateHandlesObserver implements ObserverInterface
{      
    /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
    protected $scopeConfig;
    
    const XML_PATH_QUICKVIEW_REMOVE_PRODUCT_IMAGE = 'brainvire_quickview/general/remove_product_image';
    const XML_PATH_QUICKVIEW_REMOVE_PRODUCT_IMAGE_THUMB = 'brainvire_quickview/general/remove_product_image_thumb';
    const XML_PATH_QUICKVIEW_REMOVE_AVAILABILITY = 'brainvire_quickview/general/remove_availability';
    
    /**
     * @param Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }
    
    /**
     * Add New Layout handle
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return self
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $layout = $observer->getData('layout');
        $fullActionName = $observer->getData('full_action_name');
        
        if ($fullActionName != 'brainvire_quickview_catalog_product_view') {
        
            return $this;
        }
        $removeProductImage = $this->scopeConfig->getValue(self::XML_PATH_QUICKVIEW_REMOVE_PRODUCT_IMAGE,  \Magento\Store\Model\ScopeInterface::SCOPE_STORE);        
        if ($removeProductImage) {
            $layout->getUpdate()->addHandle('brainvire_quickview_removeproduct_image');
        }
        
        $removeProductImageThumb = $this->scopeConfig->getValue(self::XML_PATH_QUICKVIEW_REMOVE_PRODUCT_IMAGE_THUMB,  \Magento\Store\Model\ScopeInterface::SCOPE_STORE);        
        if ($removeProductImageThumb) {
            $layout->getUpdate()->addHandle('brainvire_quickview_removeproduct_image_thumb');
        }
        
        $removeAvailability = $this->scopeConfig->getValue(self::XML_PATH_QUICKVIEW_REMOVE_AVAILABILITY,  \Magento\Store\Model\ScopeInterface::SCOPE_STORE);        
        if ($removeAvailability) {
            $layout->getUpdate()->addHandle('brainvire_quickview_removeavailability');
        }
       
        return $this;
    }
}
